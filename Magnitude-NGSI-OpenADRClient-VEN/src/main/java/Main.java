import org.openadr.oadr_2_0b._2012._07.OadrPayload;
import org.openadr.oadr_2_0b._2012._07.OadrPollType;
import org.openadr.oadr_2_0b._2012._07.OadrQueryRegistrationType;
import org.openadr.oadr_2_0b._2012._07.OadrSignedObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.eng.oadr.openadrclient.Oadr20bClient;

/**
 *
 * @author Engineering
 */
public class Main {

    private static final Logger log = LoggerFactory.getLogger(Main.class);

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            OadrPayload input = new OadrPayload();
            input.setOadrSignedObject(new OadrSignedObject());
            input.getOadrSignedObject().setOadrQueryRegistration(new OadrQueryRegistrationType());
            input.getOadrSignedObject().getOadrQueryRegistration().setSchemaVersion("2.0b");
            input.getOadrSignedObject().getOadrQueryRegistration().setRequestID("3a7b424e9b");
            
            OadrPayload output = Oadr20bClient.getInstance().postEiRegisterParty(input);
            log.debug(output.getOadrSignedObject().getOadrCreatedPartyRegistration().getEiResponse().getRequestID());
            log.debug(output.getOadrSignedObject().getOadrCreatedPartyRegistration().getOadrRequestedOadrPollFreq().getDuration());

            OadrPayload in = new OadrPayload();
            in.setOadrSignedObject(new OadrSignedObject());
            in.getOadrSignedObject().setOadrPoll(new OadrPollType());
            in.getOadrSignedObject().getOadrPoll().setSchemaVersion("2.0b");
            in.getOadrSignedObject().getOadrPoll().setVenID("e6ba9dd331af91dd9dfc");
            
            OadrPayload out = Oadr20bClient.getInstance().postOadrPoll(in);
            
            
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }

    }
    
}
