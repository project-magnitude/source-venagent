package it.eng.oadr.ngsiagent.services;

import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import it.eng.oadr.ngsiagent.model.EntityOadrPayload;
import it.eng.oadr.ngsiagent.model.Notification;
import it.eng.oadr.ngsiagent.utilities.AgentProperties;

@Path("CBtoVEN")
public class CBtoVEN {

	private static final Logger log = LoggerFactory.getLogger(CBtoVEN.class);

	@POST
	@Path("genericOADRPayload")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public synchronized void postGenericOADRPayload(Notification myNotification) {
		log.debug("-> postGenericOADRPayload");

		GsonBuilder gb = new GsonBuilder();
		Gson gson = gb.create();
		String record = gson.toJson(myNotification);
		log.debug("myNotification: " + myNotification);

		EntityOadrPayload myEntity = myNotification.getData().get(0);
		// estrazione payload OADR
		String myType = myEntity.getType();
		log.debug("myType: " + myType);
		String myOadrPayload = decodeOadrPayload(myEntity.getOadrPayload().getValue());
		log.debug("decoded myOadrPayload: " + myOadrPayload);

		// SET URL Suffix
		String urlSuffix = "";
		switch (myType) {
		case "MsgEiRegisterPartyToVEN":
			urlSuffix = "/EiRegisterParty";
			break;
		case "MsgEiEventToVEN":
			urlSuffix = "/EiEvent";
			break;
		case "MsgEiReportToVEN":
			urlSuffix = "/EiReport";
			break;
		case "MsgEiOptToVEN":
			urlSuffix = "/EiOpt";
			break;
		case "MsgOadrPollToVEN":
			urlSuffix = "/OadrPoll";
			break;
		}
		log.debug("urlSuffix: " + urlSuffix);
		try {

			URL url = new URL(AgentProperties.getProperty("UrlVenServer") + urlSuffix);

			log.debug("urlVENServer: " + url.toString());
			HttpURLConnection connService = (HttpURLConnection) url.openConnection();
			connService.setDoOutput(true);
			connService.setRequestMethod("POST");
			connService.setRequestProperty("Accept", "application/xml");
			connService.setRequestProperty("Content-Type", "application/xml");

			OutputStream os = connService.getOutputStream();
			os.write(myOadrPayload.getBytes());

			int myResponseCode = connService.getResponseCode();
			log.debug("HTTP error code :" + myResponseCode);

				
			connService.disconnect();
		} catch (

		Exception e) {
			e.printStackTrace();
		}

		log.debug("<- postGenericOADRPayload");
	}

	public synchronized String decodeOadrPayload(String oadrPayload) {
		log.debug("-> decodeOadrPayload");
		log.debug("oadrPayload: " + oadrPayload);
		String ret = oadrPayload;

		ret = ret.replace("#60$", "<");
		ret = ret.replace("#62$", ">");
		ret = ret.replace("#34$", "\"");
		ret = ret.replace("#39$", "\'");
		ret = ret.replace("#61$", "=");
		ret = ret.replace("#59$", ";");
		ret = ret.replace("#40$", "(");
		ret = ret.replace("#41$", "(");

		log.debug("<- decodeOadrPayload");
		return ret;
	}
}