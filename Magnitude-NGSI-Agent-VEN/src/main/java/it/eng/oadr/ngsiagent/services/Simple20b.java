package it.eng.oadr.ngsiagent.services;

import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBException;

import org.oasis_open.docs.ns.energyinterop._201110.EiResponseType;
import org.oasis_open.docs.ns.energyinterop._201110.IntervalType;
import org.oasis_open.docs.ns.energyinterop._201110.PayloadFloatType;
import org.oasis_open.docs.ns.energyinterop._201110.ReportPayloadType;
import org.openadr.oadr_2_0a._2012._07.Intervals;
import org.openadr.oadr_2_0b._2012._07.BaseUnitType;
import org.openadr.oadr_2_0b._2012._07.OadrPayload;
import org.openadr.oadr_2_0b._2012._07.OadrReportType;
import org.openadr.oadr_2_0b._2012._07.OadrResponseType;
import org.openadr.oadr_2_0b._2012._07.OadrSignedObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import ietf.params.xml.ns.icalendar_2_0.stream.StreamPayloadBaseType;
import it.eng.oadr.ngsiagent.model.AttributeOadrPayload;
import it.eng.oadr.ngsiagent.model.EntityActualFlexibilityNew;
import it.eng.oadr.ngsiagent.model.EntityActualFlexibilityOld;
//import it.eng.oadr.ngsiagent.model.EntityBidResultOld;
import it.eng.oadr.ngsiagent.model.EntityFlexibilityOfferedNew;
import it.eng.oadr.ngsiagent.model.EntityFlexibilityOfferedOld;
//import it.eng.oadr.ngsiagent.model.EntityBidResult;
import it.eng.oadr.ngsiagent.model.EntityNGSI;
import it.eng.oadr.ngsiagent.model.EntityOadrPayload;
import it.eng.oadr.ngsiagent.utilities.AgentProperties;
import it.eng.oadr.ngsiagent.utilities.Counters;
import it.eng.oadr.ngsiagent.utilities.TokenPEP;
import it.eng.oadr.openadrmodel.Parser;

import javax.xml.bind.JAXBElement;

/**
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 */
@Path("Simple/2.0b")
public class Simple20b {

	private static final Logger log = LoggerFactory.getLogger(Simple20b.class);
	private static final int HTTP_error_code_Unauthorized = 401;

	@POST
	@Path("EiRegisterParty")
	@Consumes(MediaType.APPLICATION_XML)
	@Produces(MediaType.APPLICATION_XML)
	public synchronized String postEiRegisterParty(String oadrPayload) {
		log.debug("-> postEiRegisterParty");
		log.debug("oadrPayload: " + oadrPayload);
		OadrPayload in = null;
		try {
			in = (OadrPayload) Parser.unmarshal(oadrPayload);
		} catch (JAXBException ex) {
			log.error(ex.getMessage(), ex);
			throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
		}

		// Crea una entità
		EntityOadrPayload myEntity = new EntityOadrPayload();
		String idEntity = "MsgEiRegisterPartyToVTN" + Integer.toString(Counters.getNewMsgEiRegisterParty());
		log.debug("idEntity: " + idEntity);
		myEntity.setId(idEntity);
		myEntity.setType("MsgEiRegisterPartyToVTN");

		AttributeOadrPayload myOadrPayload = new AttributeOadrPayload();
		myOadrPayload.setType("String");
		myOadrPayload.setValue(encodeOadrPayload(oadrPayload));
		myEntity.setOadrPayload(myOadrPayload);

		// Chiama il metodo per la post verso il Context Broker
		postEntityContextBroker(myEntity, "/EiRegisterParty");

		OadrPayload out = new OadrPayload();
		out.setOadrSignedObject(new OadrSignedObject());
		if (in != null && in.getOadrSignedObject() != null) {
			out.getOadrSignedObject().setOadrResponse(createResponseType());
		}

		String ret = null;
		try {
			ret = Parser.marshal(out);
			log.debug("output: " + ret);
		} catch (JAXBException ex) {
			log.error(ex.getMessage(), ex);
			throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
		}
		log.debug("<- postEiRegisterParty");
		return ret;
	}

	@POST
	@Path("EiEvent")
	@Consumes(MediaType.APPLICATION_XML)
	@Produces(MediaType.APPLICATION_XML)
	public synchronized String postEiEvent(String oadrPayload) {
		log.debug("-> postEiEvent");
		log.debug("oadrPayload: " + oadrPayload);
		OadrPayload in = null;
		try {
			in = (OadrPayload) Parser.unmarshal(oadrPayload);
		} catch (JAXBException ex) {
			log.error(ex.getMessage(), ex);
			throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
		}

		// Crea una entità
		EntityOadrPayload myEntity = new EntityOadrPayload();
		String idEntity = "MsgEiEventToVTN" + Integer.toString(Counters.getNewMsgEiEvent());
		log.debug("idEntity: " + idEntity);
		myEntity.setId(idEntity);
		myEntity.setType("MsgEiEventToVTN");

		AttributeOadrPayload myOadrPayload = new AttributeOadrPayload();
		myOadrPayload.setType("String");
		myOadrPayload.setValue(encodeOadrPayload(oadrPayload));
		myEntity.setOadrPayload(myOadrPayload);

		// Chiama il metodo per la post verso il Context Broker
		postEntityContextBroker(myEntity, "/EiEvent");

		OadrPayload out = new OadrPayload();
		out.setOadrSignedObject(new OadrSignedObject());
		if (in != null && in.getOadrSignedObject() != null) {
			out.getOadrSignedObject().setOadrResponse(createResponseType());
		}

		String ret = null;
		try {
			ret = Parser.marshal(out);
			log.debug("output: " + ret);
		} catch (JAXBException ex) {
			log.error(ex.getMessage(), ex);
			throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
		}
		log.debug("<- postEiEvent");
		return ret;
	}

	@POST
	@Path("EiReport")
	@Consumes(MediaType.APPLICATION_XML)
	@Produces(MediaType.APPLICATION_XML)
	public synchronized String postEiReport(String oadrPayload) {
		log.debug("-> postEiReport");
		log.debug("oadrPayload: " + oadrPayload);
		OadrPayload in = null;
		try {
			in = (OadrPayload) Parser.unmarshal(oadrPayload);
		} catch (JAXBException ex) {
			log.error(ex.getMessage(), ex);
			throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
		}

		sendEntityActualFlexibility(in);

		sendEntityFlexibilityOffered(in);
		
		//sendEntityBidResult(in);

		// Crea una entità
		EntityOadrPayload myEntity = new EntityOadrPayload();
		String idEntity = "MsgEiReportToVTN" + Integer.toString(Counters.getNewMsgEiReport());
		log.debug("idEntity: " + idEntity);
		myEntity.setId(idEntity);
		myEntity.setType("MsgEiReportToVTN");

		AttributeOadrPayload myOadrPayload = new AttributeOadrPayload();
		myOadrPayload.setType("String");
		myOadrPayload.setValue(encodeOadrPayload(oadrPayload));
		myEntity.setOadrPayload(myOadrPayload);

		// Chiama il metodo per la post verso il Context Broker
		postEntityContextBroker(myEntity, "/EiReport");

		OadrPayload out = new OadrPayload();
		out.setOadrSignedObject(new OadrSignedObject());
		if (in != null && in.getOadrSignedObject() != null) {
			out.getOadrSignedObject().setOadrResponse(createResponseType());
		}

		String ret = null;
		try {
			ret = Parser.marshal(out);
			log.debug("output: " + ret);
		} catch (JAXBException ex) {
			log.error(ex.getMessage(), ex);
			throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
		}
		log.debug("<- postEiReport");
		return ret;
	}

	@POST
	@Path("EiOpt")
	@Consumes(MediaType.APPLICATION_XML)
	@Produces(MediaType.APPLICATION_XML)
	public synchronized String postEiOpt(String oadrPayload) {
		log.debug("-> postEiOpt");
		log.debug("oadrPayload: " + oadrPayload);
		OadrPayload in = null;
		try {
			in = (OadrPayload) Parser.unmarshal(oadrPayload);
		} catch (JAXBException ex) {
			log.error(ex.getMessage(), ex);
			throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
		}

		// Crea una entità
		EntityOadrPayload myEntity = new EntityOadrPayload();
		String idEntity = "MsgEiOptToVTN" + Integer.toString(Counters.getNewMsgEiOpt());
		log.debug("idEntity: " + idEntity);
		myEntity.setId(idEntity);
		myEntity.setType("MsgEiOptToVTN");

		AttributeOadrPayload myOadrPayload = new AttributeOadrPayload();
		myOadrPayload.setType("String");
		myOadrPayload.setValue(encodeOadrPayload(oadrPayload));
		myEntity.setOadrPayload(myOadrPayload);

		// Chiama il metodo per la post verso il Context Broker
		postEntityContextBroker(myEntity, "/EiOpt");

		OadrPayload out = new OadrPayload();
		out.setOadrSignedObject(new OadrSignedObject());
		if (in != null && in.getOadrSignedObject() != null) {
			out.getOadrSignedObject().setOadrResponse(createResponseType());
		}

		String ret = null;
		try {
			ret = Parser.marshal(out);
			log.debug("output: " + ret);
		} catch (JAXBException ex) {
			log.error(ex.getMessage(), ex);
			throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
		}
		log.debug("<- postEiOpt");
		return ret;
	}

	@POST
	@Path("OadrPoll")
	@Consumes(MediaType.APPLICATION_XML)
	@Produces(MediaType.APPLICATION_XML)
	public synchronized String postOadrPoll(String oadrPayload) {
		log.debug("-> postOadrPoll");
		log.debug("oadrPayload: " + oadrPayload);
		OadrPayload in = null;
		try {
			in = (OadrPayload) Parser.unmarshal(oadrPayload);
		} catch (JAXBException ex) {
			log.error(ex.getMessage(), ex);
			throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
		}

		// Crea una entità
		EntityOadrPayload myEntity = new EntityOadrPayload();
		String idEntity = "MsgOadrPollToVTN" + Integer.toString(Counters.getNewMsgOadrPoll());
		log.debug("idEntity: " + idEntity);
		myEntity.setId(idEntity);
		myEntity.setType("MsgOadrPollToVTN");

		AttributeOadrPayload myOadrPayload = new AttributeOadrPayload();
		myOadrPayload.setType("String");
		myOadrPayload.setValue(encodeOadrPayload(oadrPayload));
		myEntity.setOadrPayload(myOadrPayload);

		// Chiama il metodo per la post verso il Context Broker
		postEntityContextBroker(myEntity, "/OadrPoll");

		OadrPayload out = new OadrPayload();
		out.setOadrSignedObject(new OadrSignedObject());
		if (in != null && in.getOadrSignedObject() != null) {
			out.getOadrSignedObject().setOadrResponse(createResponseType());
		}

		String ret = null;
		try {
			ret = Parser.marshal(out);
			log.debug("output: " + ret);
		} catch (JAXBException ex) {
			log.error(ex.getMessage(), ex);
			throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
		}
		log.debug("<- postOadrPoll");
		return ret;
	}

	public OadrResponseType createResponseType() {
		log.debug("-> createResponseType");
		EiResponseType eiResponse = new EiResponseType();
		eiResponse.setResponseCode("200");
		eiResponse.setResponseDescription("OK");
		OadrResponseType ret = new OadrResponseType();
		ret.setEiResponse(eiResponse);
		try {
			ret.setSchemaVersion(AgentProperties.getProperty("schemaVersion"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		log.debug("<- createResponseType");
		return ret;
	}

	public void patchEntityContextBroker(Object myEntity, String myId, String fiwareServicePath) {

		log.debug("-> patchEntityContextBroker");
		log.debug("myEntity:" + myEntity);
		log.debug("myEntity:" + myId);
		log.debug("fiwareServicePath:" + fiwareServicePath);
		boolean tokenWasUnauthorized;

		try {

			do {
				URL url = new URL(AgentProperties.getProperty("UrlContextBroker") + "/v2/entities/" + myId + "/attrs");
				log.debug("urlContextBroker: " + url.toString());
				String tokenPEP = TokenPEP.getCurrentTokenPEP();
				log.debug("tokenPEP: " + tokenPEP);
				String fiwareService = AgentProperties.getProperty("FiwareService");
				log.debug("fiwareService: " + fiwareService);
				HttpURLConnection connService = (HttpURLConnection) url.openConnection();
				connService.setDoOutput(true);
				connService.setRequestMethod("POST");
				connService.setRequestProperty("X-HTTP-Method-Override", "PATCH");
				connService.setRequestProperty("fiware-service", fiwareService);
				connService.setRequestProperty("fiware-servicepath", fiwareServicePath);
				connService.setRequestProperty("Accept", "application/json");
				connService.setRequestProperty("Content-Type", "application/json");
				if (TokenPEP.isUseTokenPEP()) {
					connService.setRequestProperty("X-Auth-Token", tokenPEP);
				}

				GsonBuilder gb = new GsonBuilder();
				gb.setDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
				Gson gson = gb.create();
				String record = gson.toJson(myEntity);
				log.debug("Rest Request: " + record);

				OutputStream os = connService.getOutputStream();
				os.write(record.getBytes());

				int myResponseCode = connService.getResponseCode();
				log.debug("HTTP error code :" + myResponseCode);

				if (TokenPEP.isUseTokenPEP() && (myResponseCode == HTTP_error_code_Unauthorized)) {
					TokenPEP.generateNewTokenPEP();
					tokenWasUnauthorized = true;
				} else {
					tokenWasUnauthorized = false;
				}
				log.debug("tokenWasUnauthorized: " + tokenWasUnauthorized);
				connService.disconnect();

			} while (tokenWasUnauthorized);

		} catch (

		Exception e) {
			e.printStackTrace();
		}
		log.debug("<- patchEntityContextBroker");
		return;
	}
	
	public void postEntityContextBroker(EntityNGSI myEntity, String fiwareServicePath) {

		log.debug("-> postEntityContextBroker");
		log.debug("myEntity:" + myEntity);
		log.debug("fiwareServicePath:" + fiwareServicePath);
		boolean tokenWasUnauthorized;

		try {

			do {
				URL url = new URL(AgentProperties.getProperty("UrlContextBroker") + "/v2/entities");
				log.debug("urlContextBroker: " + url.toString());
				String tokenPEP = TokenPEP.getCurrentTokenPEP();
				log.debug("tokenPEP: " + tokenPEP);
				String fiwareService = AgentProperties.getProperty("FiwareService");
				log.debug("fiwareService: " + fiwareService);
				HttpURLConnection connService = (HttpURLConnection) url.openConnection();
				connService.setDoOutput(true);
				connService.setRequestMethod("POST");
				connService.setRequestProperty("fiware-service", fiwareService);
				connService.setRequestProperty("fiware-servicepath", fiwareServicePath);
				connService.setRequestProperty("Accept", "application/json");
				connService.setRequestProperty("Content-Type", "application/json");
				if (TokenPEP.isUseTokenPEP()) {
					connService.setRequestProperty("X-Auth-Token", tokenPEP);
				}

				GsonBuilder gb = new GsonBuilder();
				gb.setDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
				Gson gson = gb.create();
				String record = gson.toJson(myEntity);
				log.debug("Rest Request: " + record);

				OutputStream os = connService.getOutputStream();
				os.write(record.getBytes());

				int myResponseCode = connService.getResponseCode();
				log.debug("HTTP error code :" + myResponseCode);

				if (TokenPEP.isUseTokenPEP() && (myResponseCode == HTTP_error_code_Unauthorized)) {
					TokenPEP.generateNewTokenPEP();
					tokenWasUnauthorized = true;
				} else {
					tokenWasUnauthorized = false;
				}
				log.debug("tokenWasUnauthorized: " + tokenWasUnauthorized);
				connService.disconnect();

			} while (tokenWasUnauthorized);

		} catch (

		Exception e) {
			e.printStackTrace();
		}
		log.debug("<- postEntityContextBroker");
		return;
	}

	public synchronized String encodeOadrPayload(String oadrPayload) {
		log.debug("-> encodeOadrPayload");
		log.debug("oadrPayload: " + oadrPayload);
		String ret = oadrPayload;

		ret = ret.replace("<", "#60$");
		ret = ret.replace(">", "#62$");
		ret = ret.replace("\"", "#34$");
		ret = ret.replace("\'", "#39$");
		ret = ret.replace("=", "#61$");
		ret = ret.replace(";", "#59$");
		ret = ret.replace("(", "#40$");
		ret = ret.replace(")", "#41$");

		log.debug("<- encodeOadrPayload");
		return ret;
	}

	public void sendEntityActualFlexibility(OadrPayload in) {
		log.debug("-> sendEntityActualFlexibility");
		log.debug("in: " + in);

		// Intercetta il report con reportName = "x-ACTUAL_FLEXIBILITY"
		OadrSignedObject outSiOb = in.getOadrSignedObject();
		log.debug("OadrUpdateReport: " + outSiOb.getOadrUpdateReport());
		if (in != null && outSiOb != null && outSiOb.getOadrUpdateReport() != null) {
			OadrReportType myOadrReport = outSiOb.getOadrUpdateReport().getOadrReport().get(0);
			log.debug("ReportName: " + myOadrReport.getReportName());
			if (myOadrReport.getReportName().equals("x-ACTUAL_FLEXIBILITY")) {
				// Inizializza la Entità di tipo "ActualFlexybility"
				//EntityActualFlexibility myEntityActualFlexibility = new EntityActualFlexibility();
				EntityActualFlexibilityNew myEntityActualFlexibility = new EntityActualFlexibilityNew();
				// Estrae i campi generali dal report e valorizza gli attributi della entità
//				String idEntityActualFlexibility = "ActualFlexibility"
//						+ Integer.toString(Counters.getNewActualFlexibility());
				String idEntityActualFlexibility = "ActualFlexibilityList";
				
 				log.debug("idEntityActualFlexibility: " + idEntityActualFlexibility);
				myEntityActualFlexibility.setId(idEntityActualFlexibility);
				myEntityActualFlexibility.setType("ActualFlexibility");

				List<IntervalType> myListIntervalType = myOadrReport.getIntervals().getInterval();

				for (IntervalType myinterval : myListIntervalType) {
					AttributeOadrPayload myAttributeResourceId = new AttributeOadrPayload();
					myAttributeResourceId.setType("String");

					JAXBElement<ReportPayloadType> myJAXBReportPayloadType = (JAXBElement<ReportPayloadType>) myinterval
							.getStreamPayloadBase().get(0);

					ReportPayloadType myReportPayloadType = myJAXBReportPayloadType.getValue();
					String myRID = myReportPayloadType.getRID();
					String[] splitRID = myRID.split("_");

					String myResourceId = splitRID[0];
					log.debug("myResourceId: " + myResourceId);

					myAttributeResourceId.setValue(myResourceId);
					myEntityActualFlexibility.setResourceId(myAttributeResourceId);

					AttributeOadrPayload myAttributeDateStart = new AttributeOadrPayload();
					myAttributeDateStart.setType("DateTime");
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
					String myDateStart = sdf
							.format(myinterval.getDtstart().getDateTime().toGregorianCalendar().getTime());
					log.debug("myDateStart: " + myDateStart);
					myAttributeDateStart.setValue(myDateStart);
					myEntityActualFlexibility.setDateStart(myAttributeDateStart);

					AttributeOadrPayload myAttributeDuration = new AttributeOadrPayload();
					myAttributeDuration.setType("String");
					String myDuration = myinterval.getDuration().getDuration();
					log.debug("myDuration: " + myDuration);
					myAttributeDuration.setValue(myDuration);
					myEntityActualFlexibility.setDuration(myAttributeDuration);

					List<JAXBElement<? extends StreamPayloadBaseType>> myListOadrReportPayload = myinterval
							.getStreamPayloadBase();

					for (Object myOadrReportPayload : myListOadrReportPayload) {
						JAXBElement<ReportPayloadType> myJAXBReportPayloadType2 = (JAXBElement<ReportPayloadType>) myOadrReportPayload;
						ReportPayloadType myReportPayloadType2 = myJAXBReportPayloadType2.getValue();
						String myRID2 = myReportPayloadType2.getRID();
						int myIndex = myResourceId.length() + 1;
						String mySuffix = myRID2.substring(myIndex);
						log.debug("mySuffix: " + mySuffix);
						switch (mySuffix) {
						case "flexibility_provider":
							AttributeOadrPayload myAttributeFlexibilityProvider = new AttributeOadrPayload();
							myAttributeFlexibilityProvider.setType("String");
							JAXBElement<PayloadFloatType> payloadFloatTypeFlexibilityProvider = (JAXBElement<PayloadFloatType>) myReportPayloadType2
									.getPayloadBase();
							float valueFloatFlexibilityProvider = payloadFloatTypeFlexibilityProvider.getValue()
									.getValue();
							// String myFlexibilityProvider = Float.toString(valueFloatFlexibilityProvider);
							String myFlexibilityProvider = Integer.toString((int) valueFloatFlexibilityProvider);
							log.debug("myFlexibilityProvider: " + myFlexibilityProvider);
							myAttributeFlexibilityProvider.setValue(myFlexibilityProvider);
							myEntityActualFlexibility.setFlexibilityProvider(myAttributeFlexibilityProvider);
							break;
						case "service_type":
							AttributeOadrPayload myAttributeServiceType = new AttributeOadrPayload();
							myAttributeServiceType.setType("String");
							JAXBElement<PayloadFloatType> payloadFloatTypeServiceType = (JAXBElement<PayloadFloatType>) myReportPayloadType2
									.getPayloadBase();
							float valueFloatServiceType = payloadFloatTypeServiceType.getValue().getValue();
							// String myServiceType = Float.toString(valueFloatServiceType);
							String myServiceType = Integer.toString((int) valueFloatServiceType);
							log.debug("myServiceType: " + myServiceType);
							myAttributeServiceType.setValue(myServiceType);
							myEntityActualFlexibility.setServiceType(myAttributeServiceType);
							break;
						case "available_power":
							AttributeOadrPayload myAttributeAvailablePower = new AttributeOadrPayload();
							myAttributeAvailablePower.setType("float");
							JAXBElement<PayloadFloatType> payloadFloatTypeAvailablePower = (JAXBElement<PayloadFloatType>) myReportPayloadType2
									.getPayloadBase();
							float valueFloatAvailablePower = payloadFloatTypeAvailablePower.getValue().getValue();
							String myAvailablePower = Float.toString(valueFloatAvailablePower);
							log.debug("myAvailablePower: " + myAvailablePower);
							myAttributeAvailablePower.setValue(myAvailablePower);
							myEntityActualFlexibility.setAvailablePower(myAttributeAvailablePower);
							break;
						case "available_energy":
							AttributeOadrPayload myAttributeAvailableEnergy = new AttributeOadrPayload();
							myAttributeAvailableEnergy.setType("float");
							JAXBElement<PayloadFloatType> payloadFloatTypeAvailableEnergy = (JAXBElement<PayloadFloatType>) myReportPayloadType2
									.getPayloadBase();
							float valueFloatAvailableEnergy = payloadFloatTypeAvailableEnergy.getValue().getValue();
							String myAvailableEnergy = Float.toString(valueFloatAvailableEnergy);
							log.debug("myAvailableEnergy: " + myAvailableEnergy);
							myAttributeAvailableEnergy.setValue(myAvailableEnergy);
							myEntityActualFlexibility.setAvailableEnergy(myAttributeAvailableEnergy);
							break;
						case "ramp_rate":
							AttributeOadrPayload myAttributeRampRate = new AttributeOadrPayload();
							myAttributeRampRate.setType("float");
							JAXBElement<PayloadFloatType> payloadFloatTypeRampRate = (JAXBElement<PayloadFloatType>) myReportPayloadType2
									.getPayloadBase();
							float valueFloatRampRate = payloadFloatTypeRampRate.getValue().getValue();
							String myRampRate = Float.toString(valueFloatRampRate);
							log.debug("myRampRate: " + myRampRate);
							myAttributeRampRate.setValue(myRampRate);
							myEntityActualFlexibility.setRampRate(myAttributeRampRate);
							break;
						case "ramp_duration":
							AttributeOadrPayload myAttributeRampDuration = new AttributeOadrPayload();
							myAttributeRampDuration.setType("float");
							JAXBElement<PayloadFloatType> payloadFloatTypeRampDuration = (JAXBElement<PayloadFloatType>) myReportPayloadType2
									.getPayloadBase();
							float valueFloatRampDuration = payloadFloatTypeRampDuration.getValue().getValue();
							String myRampDuration = Float.toString(valueFloatRampDuration);
							log.debug("myRampDuration: " + myRampDuration);
							myAttributeRampDuration.setValue(myRampDuration);
							myEntityActualFlexibility.setRampDuration(myAttributeRampDuration);
							break;
						case "start_time":
							AttributeOadrPayload myAttributeStartTime = new AttributeOadrPayload();
							myAttributeStartTime.setType("int");
							JAXBElement<PayloadFloatType> payloadFloatTypeStartTime = (JAXBElement<PayloadFloatType>) myReportPayloadType2
									.getPayloadBase();
							float valueFloatStartTime = payloadFloatTypeStartTime.getValue().getValue();
							String myStartTime = Integer.toString((int) valueFloatStartTime);

							log.debug("myStartTime: " + myStartTime);
							myAttributeStartTime.setValue(myStartTime);
							myEntityActualFlexibility.setStartTime(myAttributeStartTime);
							break;
						case "end_time":
							AttributeOadrPayload myAttributeEndTime = new AttributeOadrPayload();
							myAttributeEndTime.setType("int");
							JAXBElement<PayloadFloatType> payloadFloatTypeEndTime = (JAXBElement<PayloadFloatType>) myReportPayloadType2
									.getPayloadBase();
							float valueFloatEndTime = payloadFloatTypeEndTime.getValue().getValue();
							String myEndTime = Integer.toString((int) valueFloatEndTime);
							log.debug("myEndTime: " + myEndTime);
							myAttributeEndTime.setValue(myEndTime);
							myEntityActualFlexibility.setEndTime(myAttributeEndTime);
							break;

						}

					}

					//postEntityContextBroker(myEntityActualFlexibility, "/ActualFlexibility");
					if (Counters.existsActualFlexibility()) {
						EntityActualFlexibilityOld myEntityActualFlexibilityOld = new EntityActualFlexibilityOld(myEntityActualFlexibility.getResourceId(),
																												 myEntityActualFlexibility.getDateStart(),
																												 myEntityActualFlexibility.getDuration(),
																												 myEntityActualFlexibility.getFlexibilityProvider(),
																												 myEntityActualFlexibility.getServiceType(),
																												 myEntityActualFlexibility.getAvailablePower(),
																												 myEntityActualFlexibility.getAvailableEnergy(),
																												 myEntityActualFlexibility.getRampRate(),
																												 myEntityActualFlexibility.getRampDuration(),
																												 myEntityActualFlexibility.getStartTime(),
																												 myEntityActualFlexibility.getEndTime());
						
						patchEntityContextBroker(myEntityActualFlexibilityOld, myEntityActualFlexibility.getId(), "/ActualFlexibility");
					} else {
						postEntityContextBroker(myEntityActualFlexibility, "/ActualFlexibility");	
						Counters.setActualFlexibility();
					}

				}

			}

		}

		log.debug("<- sendEntityActualFlexibility");
		return;
	}

	public void sendEntityFlexibilityOffered(OadrPayload in) {
		log.debug("-> sendEntityFlexibilityOffered");
		log.debug("in: " + in);

		// Intercetta il report con reportName = "x-FLEXIBILITY_OFFERED"
		OadrSignedObject outSiOb = in.getOadrSignedObject();
		log.debug("OadrUpdateReport: " + outSiOb.getOadrUpdateReport());
		if (in != null && outSiOb != null && outSiOb.getOadrUpdateReport() != null) {
			OadrReportType myOadrReport = outSiOb.getOadrUpdateReport().getOadrReport().get(0);
			log.debug("ReportName: " + myOadrReport.getReportName());
			if (myOadrReport.getReportName().equals("x-FLEXIBILITY_OFFERED")) {
				// Inizializza la Entità di tipo "x-FLEXIBILITY_OFFERED"
				//EntityFlexibilityOffered myEntityFlexibilityOffered = new EntityFlexibilityOffered();
				EntityFlexibilityOfferedNew myEntityFlexibilityOffered = new EntityFlexibilityOfferedNew();
				// Estrae i campi generali dal report e valorizza gli attributi della entità
//				String idEntityFlexibilityOffered = "FlexibilityOffered"
//						+ Integer.toString(Counters.getNewFlexibilityOffered());
				String idEntityFlexibilityOffered = "FlexibilityOfferedList";
								
				log.debug("idEntityFlexibilityOffered: " + idEntityFlexibilityOffered);
				myEntityFlexibilityOffered.setId(idEntityFlexibilityOffered);
				myEntityFlexibilityOffered.setType("FlexibilityOffered");

				List<IntervalType> myListIntervalType = myOadrReport.getIntervals().getInterval();

				for (IntervalType myinterval : myListIntervalType) {
					AttributeOadrPayload myAttributeResourceId = new AttributeOadrPayload();
					myAttributeResourceId.setType("String");
					JAXBElement<ReportPayloadType> myJAXBReportPayloadType = (JAXBElement<ReportPayloadType>) myinterval
							.getStreamPayloadBase().get(0);
					ReportPayloadType myReportPayloadType = myJAXBReportPayloadType.getValue();
					String myRID = myReportPayloadType.getRID();
					String[] splitRID = myRID.split("_");
					String myResourceId = splitRID[0];
					log.debug("myResourceId: " + myResourceId);
					myAttributeResourceId.setValue(myResourceId);
					myEntityFlexibilityOffered.setResourceId(myAttributeResourceId);

					AttributeOadrPayload myAttributeDateStart = new AttributeOadrPayload();
					myAttributeDateStart.setType("DateTime");
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
					String myDateStart = sdf
							.format(myinterval.getDtstart().getDateTime().toGregorianCalendar().getTime());
					log.debug("myDateStart: " + myDateStart);
					myAttributeDateStart.setValue(myDateStart);
					myEntityFlexibilityOffered.setDateStart(myAttributeDateStart);

					AttributeOadrPayload myAttributeDuration = new AttributeOadrPayload();
					myAttributeDuration.setType("String");
					String myDuration = myinterval.getDuration().getDuration();
					log.debug("myDuration: " + myDuration);
					myAttributeDuration.setValue(myDuration);
					myEntityFlexibilityOffered.setDuration(myAttributeDuration);

					List<JAXBElement<? extends StreamPayloadBaseType>> myListOadrReportPayload = myinterval
							.getStreamPayloadBase();

					for (Object myOadrReportPayload : myListOadrReportPayload) {
						JAXBElement<ReportPayloadType> myJAXBReportPayloadType2 = (JAXBElement<ReportPayloadType>) myOadrReportPayload;
						ReportPayloadType myReportPayloadType2 = myJAXBReportPayloadType2.getValue();
						String myRID2 = myReportPayloadType2.getRID();
						int myIndex = myResourceId.length() + 1;
						String mySuffix = myRID2.substring(myIndex);
						log.debug("mySuffix: " + mySuffix);
						switch (mySuffix) {
						case "flexibility_price":
							AttributeOadrPayload myAttributeFlexibilityPrice = new AttributeOadrPayload();
							myAttributeFlexibilityPrice.setType("float");
							JAXBElement<PayloadFloatType> payloadFloatTypeFlexibilityPrice = (JAXBElement<PayloadFloatType>) myReportPayloadType2
									.getPayloadBase();
							float valueFloatFlexibilityPrice = payloadFloatTypeFlexibilityPrice.getValue().getValue();
							String myFlexibilityPrice = Float.toString(valueFloatFlexibilityPrice);
							log.debug("myFlexibilityPrice: " + myFlexibilityPrice);
							myAttributeFlexibilityPrice.setValue(myFlexibilityPrice);
							myEntityFlexibilityOffered.setFlexibilityPrice(myAttributeFlexibilityPrice);
							break;
						case "flexibility_offered":
							AttributeOadrPayload myAttributeFlexibilityOffered = new AttributeOadrPayload();
							myAttributeFlexibilityOffered.setType("float");
							JAXBElement<PayloadFloatType> payloadFloatTypeFlexibilityOffered = (JAXBElement<PayloadFloatType>) myReportPayloadType2
									.getPayloadBase();
							float valueFloatFlexibilityOffered = payloadFloatTypeFlexibilityOffered.getValue()
									.getValue();
							String myFlexibilityOffered = Float.toString(valueFloatFlexibilityOffered);
							log.debug("myFlexibilityOffered: " + myFlexibilityOffered);
							myAttributeFlexibilityOffered.setValue(myFlexibilityOffered);
							myEntityFlexibilityOffered.setFlexibilityOffered(myAttributeFlexibilityOffered);
							break;
						case "flexibility_accepted":
							AttributeOadrPayload myAttributeFlexibilityAccepted = new AttributeOadrPayload();
							myAttributeFlexibilityAccepted.setType("float");
							JAXBElement<PayloadFloatType> payloadFloatTypeFlexibilityAccepted = (JAXBElement<PayloadFloatType>) myReportPayloadType2
									.getPayloadBase();
							float valueFloatFlexibilityAccepted = payloadFloatTypeFlexibilityAccepted.getValue()
									.getValue();
							String myFlexibilityAccepted = Float.toString(valueFloatFlexibilityAccepted);
							log.debug("myFlexibilityAccepted: " + myFlexibilityAccepted);
							myAttributeFlexibilityAccepted.setValue(myFlexibilityAccepted);
							myEntityFlexibilityOffered.setFlexibilityAccepted(myAttributeFlexibilityAccepted);
							break;
						case "reference_bid":
							AttributeOadrPayload myAttributeReferenceBid = new AttributeOadrPayload();
							myAttributeReferenceBid.setType("int");
							JAXBElement<PayloadFloatType> payloadFloatTypeReferenceBid = (JAXBElement<PayloadFloatType>) myReportPayloadType2
									.getPayloadBase();
							float valueFloatReferenceBid = payloadFloatTypeReferenceBid.getValue().getValue();
							String myReferenceBid = Integer.toString((int) valueFloatReferenceBid);
							log.debug("myReferenceBid: " + myReferenceBid);
							myAttributeReferenceBid.setValue(myReferenceBid);
							myEntityFlexibilityOffered.setReferenceBid(myAttributeReferenceBid);
							break;

						}

					}

					//postEntityContextBroker(myEntityFlexibilityOffered, "/FlexibilityOffered");
					if (Counters.existsFlexibilityOffered()) {
						EntityFlexibilityOfferedOld myEntityFlexibilityOfferedOld = new EntityFlexibilityOfferedOld(myEntityFlexibilityOffered.getResourceId(),
																													myEntityFlexibilityOffered.getDateStart(),
																													myEntityFlexibilityOffered.getDuration(),
																													myEntityFlexibilityOffered.getFlexibilityPrice(),
																													myEntityFlexibilityOffered.getFlexibilityOffered(),
																													myEntityFlexibilityOffered.getFlexibilityAccepted(),
																													myEntityFlexibilityOffered.getReferenceBid());
						
						patchEntityContextBroker(myEntityFlexibilityOfferedOld, myEntityFlexibilityOffered.getId(), "/FlexibilityOffered");
					} else {
						postEntityContextBroker(myEntityFlexibilityOffered, "/FlexibilityOffered");	
						Counters.setFlexibilityOffered();
					}

				}

			}

		}

		log.debug("<- sendEntityFlexibilityOffered");
		return;
	}
	
//	public void sendEntityBidResult(OadrPayload in) {
//		log.debug("-> sendEntityBidResult");
//		log.debug("in: " + in);
//
//		// Intercetta il report con reportName = "x-BID_RESULT"
//		OadrSignedObject outSiOb = in.getOadrSignedObject();
//		log.debug("OadrUpdateReport: " + outSiOb.getOadrUpdateReport());
//		if (in != null && outSiOb != null && outSiOb.getOadrUpdateReport() != null) {
//			OadrReportType myOadrReport = outSiOb.getOadrUpdateReport().getOadrReport().get(0);
//			log.debug("ReportName: " + myOadrReport.getReportName());
//			if (myOadrReport.getReportName().equals("x-BID_RESULT")) {
//				// Inizializza la Entità di tipo "x-FLEXIBILITY_OFFERED"
//				EntityBidResult myEntityBidResult = new EntityBidResult();
//				// Estrae i campi generali dal report e valorizza gli attributi della entità
//				String idEntityBidResult = "BidResult"
//						+ Integer.toString(Counters.getNewBidResult());
//				log.debug("idEntityBidResult: " + idEntityBidResult);
//				myEntityBidResult.setId(idEntityBidResult);
//				myEntityBidResult.setType("BidResult");
//
//				List<IntervalType> myListIntervalType = myOadrReport.getIntervals().getInterval();
//
//				for (IntervalType myinterval : myListIntervalType) {
//					AttributeOadrPayload myAttributeResourceId = new AttributeOadrPayload();
//					myAttributeResourceId.setType("String");
//					JAXBElement<ReportPayloadType> myJAXBReportPayloadType = (JAXBElement<ReportPayloadType>) myinterval
//							.getStreamPayloadBase().get(0);
//					ReportPayloadType myReportPayloadType = myJAXBReportPayloadType.getValue();
//					String myRID = myReportPayloadType.getRID();
//					String[] splitRID = myRID.split("_");
//					String myResourceId = splitRID[0];
//					log.debug("myResourceId: " + myResourceId);
//					myAttributeResourceId.setValue(myResourceId);
//					myEntityBidResult.setResourceId(myAttributeResourceId);
//
//					AttributeOadrPayload myAttributeDateStart = new AttributeOadrPayload();
//					myAttributeDateStart.setType("DateTime");
//					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
//					String myDateStart = sdf
//							.format(myinterval.getDtstart().getDateTime().toGregorianCalendar().getTime());
//					log.debug("myDateStart: " + myDateStart);
//					myAttributeDateStart.setValue(myDateStart);
//					myEntityBidResult.setDateStart(myAttributeDateStart);
//
//					AttributeOadrPayload myAttributeDuration = new AttributeOadrPayload();
//					myAttributeDuration.setType("String");
//					String myDuration = myinterval.getDuration().getDuration();
//					log.debug("myDuration: " + myDuration);
//					myAttributeDuration.setValue(myDuration);
//					myEntityBidResult.setDuration(myAttributeDuration);
//
//					List<JAXBElement<? extends StreamPayloadBaseType>> myListOadrReportPayload = myinterval
//							.getStreamPayloadBase();
//
//					for (Object myOadrReportPayload : myListOadrReportPayload) {
//						JAXBElement<ReportPayloadType> myJAXBReportPayloadType2 = (JAXBElement<ReportPayloadType>) myOadrReportPayload;
//						ReportPayloadType myReportPayloadType2 = myJAXBReportPayloadType2.getValue();
//						String myRID2 = myReportPayloadType2.getRID();
//						int myIndex = myResourceId.length() + 1;
//						String mySuffix = myRID2.substring(myIndex);
//						log.debug("mySuffix: " + mySuffix);
//						switch (mySuffix) {
//						case "flexibility_price":
//							AttributeOadrPayload myAttributeFlexibilityPrice = new AttributeOadrPayload();
//							myAttributeFlexibilityPrice.setType("float");
//							JAXBElement<PayloadFloatType> payloadFloatTypeFlexibilityPrice = (JAXBElement<PayloadFloatType>) myReportPayloadType2
//									.getPayloadBase();
//							float valueFloatFlexibilityPrice = payloadFloatTypeFlexibilityPrice.getValue().getValue();
//							String myFlexibilityPrice = Float.toString(valueFloatFlexibilityPrice);
//							log.debug("myFlexibilityPrice: " + myFlexibilityPrice);
//							myAttributeFlexibilityPrice.setValue(myFlexibilityPrice);
//							myEntityBidResult.setFlexibilityPrice(myAttributeFlexibilityPrice);
//							break;
//						case "flexibility_offered":
//							AttributeOadrPayload myAttributeFlexibilityOffered = new AttributeOadrPayload();
//							myAttributeFlexibilityOffered.setType("float");
//							JAXBElement<PayloadFloatType> payloadFloatTypeFlexibilityOffered = (JAXBElement<PayloadFloatType>) myReportPayloadType2
//									.getPayloadBase();
//							float valueFloatFlexibilityOffered = payloadFloatTypeFlexibilityOffered.getValue()
//									.getValue();
//							String myFlexibilityOffered = Float.toString(valueFloatFlexibilityOffered);
//							log.debug("myFlexibilityOffered: " + myFlexibilityOffered);
//							myAttributeFlexibilityOffered.setValue(myFlexibilityOffered);
//							myEntityBidResult.setFlexibilityOffered(myAttributeFlexibilityOffered);
//							break;
//						case "flexibility_accepted":
//							AttributeOadrPayload myAttributeFlexibilityAccepted = new AttributeOadrPayload();
//							myAttributeFlexibilityAccepted.setType("float");
//							JAXBElement<PayloadFloatType> payloadFloatTypeFlexibilityAccepted = (JAXBElement<PayloadFloatType>) myReportPayloadType2
//									.getPayloadBase();
//							float valueFloatFlexibilityAccepted = payloadFloatTypeFlexibilityAccepted.getValue()
//									.getValue();
//							String myFlexibilityAccepted = Float.toString(valueFloatFlexibilityAccepted);
//							log.debug("myFlexibilityAccepted: " + myFlexibilityAccepted);
//							myAttributeFlexibilityAccepted.setValue(myFlexibilityAccepted);
//							myEntityBidResult.setFlexibilityAccepted(myAttributeFlexibilityAccepted);
//							break;
//						case "reference_bid":
//							AttributeOadrPayload myAttributeReferenceBid = new AttributeOadrPayload();
//							myAttributeReferenceBid.setType("int");
//							JAXBElement<PayloadFloatType> payloadFloatTypeReferenceBid = (JAXBElement<PayloadFloatType>) myReportPayloadType2
//									.getPayloadBase();
//							float valueFloatReferenceBid = payloadFloatTypeReferenceBid.getValue().getValue();
//							String myReferenceBid = Integer.toString((int) valueFloatReferenceBid);
//							log.debug("myReferenceBid: " + myReferenceBid);
//							myAttributeReferenceBid.setValue(myReferenceBid);
//							myEntityBidResult.setReferenceBid(myAttributeReferenceBid);
//							break;
//
//						}
//
//					}
//
//					postEntityContextBroker(myEntityBidResult, "/BidResult");
//
//				}
//
//			}
//
//		}
//
//		log.debug("<- sendEntityBidResult");
//		return;
//	}

}