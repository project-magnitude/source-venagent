package it.eng.oadr.ngsiagent.model;

import java.util.ArrayList;

/**
 * Entity Model
 */
public class Notification {

	private String subscriptionId;
	private ArrayList<EntityOadrPayload> data;

	public ArrayList<EntityOadrPayload> getData() {
		return data;
	}

	public void setData(ArrayList<EntityOadrPayload> data) {
		this.data = data;
	}

	public String getSubscriptionId() {
		return subscriptionId;
	}

	public Notification(String subscriptionId, ArrayList<EntityOadrPayload> data) {
		this.subscriptionId = subscriptionId;
		this.data = data;
	}

	public void setSubscriptionId(String subscriptionId) {
		this.subscriptionId = subscriptionId;
	}

	public Notification() {
	}

}
