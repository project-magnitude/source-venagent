package it.eng.oadr.ngsiagent.services;

import javax.annotation.PostConstruct;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@ApplicationPath("/OpenADR2")
public class RestApplication extends Application{

	private static final Logger logger = LoggerFactory.getLogger(RestApplication.class);

	@PostConstruct
	public void init() {
		logger.debug("RestApplication inited");
	}
}
