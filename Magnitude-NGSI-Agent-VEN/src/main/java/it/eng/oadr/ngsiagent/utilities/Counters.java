package it.eng.oadr.ngsiagent.utilities;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class Counters {

	private static final Logger log = LoggerFactory.getLogger(Counters.class);

	private static int MsgEiRegisterParty;
	private static int MsgEiEvent;
	private static int MsgEiReport;
	private static int MsgEiOpt;
	private static int MsgOadrPoll;
	private static int ActualFlexibility;
	private static int FlexibilityOffered;
	//private static int BidResult;
	//private static final int HTTP_error_code_Unauthorized = 401;
	private static final int Counter_Singleton_Exists = 1;
	private static final int Counter_Singleton_Not_Exists = 0;

//	static {
//		try {
//			MsgEiRegisterParty = 0;
//			String counterMsgEiRegisterParty = AgentProperties.getProperty("CounterMsgEiRegisterParty");
//			if (!counterMsgEiRegisterParty.isEmpty()) {
//				MsgEiRegisterParty = Integer.valueOf(counterMsgEiRegisterParty).intValue();
//			}
//
//			MsgEiEvent = 0;
//			String counterMsgEiEvent = AgentProperties.getProperty("CounterMsgEiEvent");
//			if (!counterMsgEiEvent.isEmpty()) {
//				MsgEiEvent = Integer.valueOf(counterMsgEiEvent).intValue();
//			}
//			MsgEiReport = 0;
//			String counterMsgEiReport = AgentProperties.getProperty("CounterMsgEiReport");
//			if (!counterMsgEiReport.isEmpty()) {
//				MsgEiReport = Integer.valueOf(counterMsgEiReport).intValue();
//			}
//			MsgEiOpt = 0;
//			String counterMsgEiOpt = AgentProperties.getProperty("CounterMsgEiOpt");
//			if (!counterMsgEiOpt.isEmpty()) {
//				MsgEiOpt = Integer.valueOf(counterMsgEiOpt).intValue();
//			}
//			MsgOadrPoll = 0;
//			String counterMsgOadrPoll = AgentProperties.getProperty("CounterMsgOadrPoll");
//			if (!counterMsgOadrPoll.isEmpty()) {
//				MsgOadrPoll = Integer.valueOf(counterMsgOadrPoll).intValue();
//			}
//
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}

	public static synchronized void syncAllCounters() {
		log.debug("--> syncAllCounters");
		MsgEiRegisterParty = syncGenericCounter("MsgEiRegisterPartyToVTN", "/EiRegisterParty");
		MsgEiEvent = syncGenericCounter("MsgEiEventToVTN", "/EiEvent");
		MsgEiReport = syncGenericCounter("MsgEiReportToVTN", "/EiReport");
		MsgEiOpt = syncGenericCounter("MsgEiOptToVTN", "/EiOpt");
		MsgOadrPoll = syncGenericCounter("MsgOadrPollToVTN", "/OadrPoll");
		//ActualFlexibility = syncGenericCounter("ActualFlexibility", "/ActualFlexibility");
		//FlexibilityOffered = syncGenericCounter("FlexibilityOffered", "/FlexibilityOffered");
		//ActualFlexibility = syncGenericCounterSingleton("ActualFlexibility", "/ActualFlexibility");
		ActualFlexibility = syncGenericCounterSingleton("ActualFlexibilityList", "/ActualFlexibility");
		//FlexibilityOffered = syncGenericCounterSingleton("FlexibilityOffered", "/FlexibilityOffered");
		FlexibilityOffered = syncGenericCounterSingleton("FlexibilityOfferedList", "/FlexibilityOffered");
		//BidResult = syncGenericCounter("BidResult", "/BidResult");

		log.debug("<-- syncAllCounters");
		return;

	}

	public static synchronized int syncGenericCounter(String type, String fiwareServicePath) {

		log.debug("--> syncGenericCounter");
		log.debug("type: " + type);
		log.debug("fiwareServicePath: " + fiwareServicePath);

		int myCounter = 0;
		String lastId = "";

		boolean tokenWasUnauthorized;
		try {

			do {
				String attr = "?type=" + type + "&orderBy=!dateCreated&limit=1";
				URL url = new URL(AgentProperties.getProperty("UrlContextBroker") + "/v2/entities" + attr);
				log.debug("urlContextBroker: " + url.toString());
				String tokenPEP = TokenPEP.getCurrentTokenPEP();
				log.debug("tokenPEP: " + tokenPEP);
				String fiwareService = AgentProperties.getProperty("FiwareService");
				log.debug("fiwareService: " + fiwareService);
				HttpURLConnection connService = (HttpURLConnection) url.openConnection();
				connService.setDoOutput(false);
				connService.setRequestMethod("GET");
				connService.setRequestProperty("fiware-service", fiwareService);
				connService.setRequestProperty("fiware-servicepath", fiwareServicePath);
				connService.setRequestProperty("Accept", "application/json");
				if (TokenPEP.isUseTokenPEP()) {
					connService.setRequestProperty("X-Auth-Token", tokenPEP);
				}

				int myResponseCode = connService.getResponseCode();
				log.debug("HTTP error code :" + myResponseCode);

				if (TokenPEP.isUseTokenPEP() && (myResponseCode == HttpURLConnection.HTTP_UNAUTHORIZED)) {
					TokenPEP.generateNewTokenPEP();
					tokenWasUnauthorized = true;
				} else {
					tokenWasUnauthorized = false;

					BufferedReader br = new BufferedReader(new InputStreamReader((connService.getInputStream())));

					StringBuilder sb = new StringBuilder();
					String line;
					while ((line = br.readLine()) != null) {
						sb.append(line);
					}
					br.close();
					String response = sb.toString();

					// JsonObject responseJson = (JsonObject) new JsonParser().parse(response);
					// lastId = responseJson.get("id").toString().replace("\"", "");

					JsonArray jsonArray = new JsonParser().parse(response).getAsJsonArray();

					for (JsonElement jsonObjectElement : jsonArray) {
						JsonObject jsonObject = jsonObjectElement.getAsJsonObject();
						lastId = jsonObject.get("id").toString().replace("\"", "");

					}

					log.debug("lastId: " + lastId);

					int myIndex = lastId.indexOf(type);
					log.debug("myIndex: " + myIndex);
					if (myIndex != -1) {
						String lastCounter = lastId.substring(myIndex + type.length());
						log.debug("lastCounter: " + lastCounter);
						myCounter = Integer.parseInt(lastCounter);
					}

					log.debug("myCounter: " + myCounter);
				}
				log.debug("tokenWasUnauthorized: " + tokenWasUnauthorized);

				connService.disconnect();

			} while (tokenWasUnauthorized);

		} catch (

		Exception e) {
			e.printStackTrace();
		}

		log.debug("<-- syncGenericCounter");

		return myCounter;

	}
	
	public static synchronized int syncGenericCounterSingleton(String id, String fiwareServicePath) {

		log.debug("--> syncGenericCounterSingleton");
		log.debug("id: " + id);
		log.debug("fiwareServicePath: " + fiwareServicePath);

		int myCounter = 0;
		String lastId = "";

		boolean tokenWasUnauthorized;
		try {

			do {
				URL url = new URL(AgentProperties.getProperty("UrlContextBroker") + "/v2/entities/" + id);
				log.debug("urlContextBroker: " + url.toString());
				String tokenPEP = TokenPEP.getCurrentTokenPEP();
				log.debug("tokenPEP: " + tokenPEP);
				String fiwareService = AgentProperties.getProperty("FiwareService");
				log.debug("fiwareService: " + fiwareService);
				HttpURLConnection connService = (HttpURLConnection) url.openConnection();
				connService.setDoOutput(false);
				connService.setRequestMethod("GET");
				connService.setRequestProperty("fiware-service", fiwareService);
				connService.setRequestProperty("fiware-servicepath", fiwareServicePath);
				connService.setRequestProperty("Accept", "application/json");
				if (TokenPEP.isUseTokenPEP()) {
					connService.setRequestProperty("X-Auth-Token", tokenPEP);
				}

				int myResponseCode = connService.getResponseCode();
				log.debug("HTTP error code :" + myResponseCode);

				if (TokenPEP.isUseTokenPEP() && (myResponseCode ==  HttpURLConnection.HTTP_UNAUTHORIZED)) {
					TokenPEP.generateNewTokenPEP();
					tokenWasUnauthorized = true;
				} else {
					tokenWasUnauthorized = false;

					if (myResponseCode == HttpURLConnection.HTTP_OK) {
						myCounter = Counter_Singleton_Exists;
					} else {
						myCounter = Counter_Singleton_Not_Exists;
					}

					log.debug("myCounter: " + myCounter);
				}
				log.debug("tokenWasUnauthorized: " + tokenWasUnauthorized);

				connService.disconnect();

			} while (tokenWasUnauthorized);

		} catch (

		Exception e) {
			e.printStackTrace();
		}

		log.debug("<-- syncGenericCounterSingleton");

		return myCounter;

	}
	
//	public static synchronized int syncGenericCounterSingleton(String type, String fiwareServicePath) {
//
//		log.debug("--> syncGenericCounterSingleton");
//		log.debug("type: " + type);
//		log.debug("fiwareServicePath: " + fiwareServicePath);
//
//		int myCounter = 0;
//		String lastId = "";
//
//		boolean tokenWasUnauthorized;
//		try {
//
//			do {
//				String attr = "?type=" + type + "&orderBy=!dateCreated&limit=1";
//				URL url = new URL(AgentProperties.getProperty("UrlContextBroker") + "/v2/entities" + attr);
//				log.debug("urlContextBroker: " + url.toString());
//				String tokenPEP = TokenPEP.getCurrentTokenPEP();
//				log.debug("tokenPEP: " + tokenPEP);
//				String fiwareService = AgentProperties.getProperty("FiwareService");
//				log.debug("fiwareService: " + fiwareService);
//				HttpURLConnection connService = (HttpURLConnection) url.openConnection();
//				connService.setDoOutput(false);
//				connService.setRequestMethod("GET");
//				connService.setRequestProperty("fiware-service", fiwareService);
//				connService.setRequestProperty("fiware-servicepath", fiwareServicePath);
//				connService.setRequestProperty("Accept", "application/json");
//				if (TokenPEP.isUseTokenPEP()) {
//					connService.setRequestProperty("X-Auth-Token", tokenPEP);
//				}
//
//				int myResponseCode = connService.getResponseCode();
//				log.debug("HTTP error code :" + myResponseCode);
//
//				if (TokenPEP.isUseTokenPEP() && (myResponseCode == HttpURLConnection.HTTP_UNAUTHORIZED)) {
//					TokenPEP.generateNewTokenPEP();
//					tokenWasUnauthorized = true;
//				} else {
//					tokenWasUnauthorized = false;
//
//					BufferedReader br = new BufferedReader(new InputStreamReader((connService.getInputStream())));
//
//					StringBuilder sb = new StringBuilder();
//					String line;
//					while ((line = br.readLine()) != null) {
//						sb.append(line);
//					}
//					br.close();
//					String response = sb.toString();
//
//					// JsonObject responseJson = (JsonObject) new JsonParser().parse(response);
//					// lastId = responseJson.get("id").toString().replace("\"", "");
//
//					JsonArray jsonArray = new JsonParser().parse(response).getAsJsonArray();
//
//					for (JsonElement jsonObjectElement : jsonArray) {
//						JsonObject jsonObject = jsonObjectElement.getAsJsonObject();
//						lastId = jsonObject.get("id").toString().replace("\"", "");
//
//					}
//
//					log.debug("lastId: " + lastId);
//
//					int myIndex = lastId.indexOf(type);
//					log.debug("myIndex: " + myIndex);
//					if (myIndex != -1) {
//						String lastCounter = lastId.substring(myIndex + type.length());
//						log.debug("lastCounter: " + lastCounter);
//						//myCounter = Integer.parseInt(lastCounter);
//						myCounter = Counter_Singleton_Exists;
//					} else {
//						myCounter = Counter_Singleton_Not_Exists;
//					}
//
//					log.debug("myCounter: " + myCounter);
//				}
//				log.debug("tokenWasUnauthorized: " + tokenWasUnauthorized);
//
//				connService.disconnect();
//
//			} while (tokenWasUnauthorized);
//
//		} catch (
//
//		Exception e) {
//			e.printStackTrace();
//		}
//
//		log.debug("<-- syncGenericCounterSingleton");
//
//		return myCounter;
//
//	}

	public static synchronized int getNewMsgEiRegisterParty() {

		return ++MsgEiRegisterParty;

	}

	public static synchronized int getNewMsgEiEvent() {

		return ++MsgEiEvent;

	}

	public static synchronized int getNewMsgEiReport() {

		return ++MsgEiReport;

	}

	public static synchronized int getNewMsgEiOpt() {

		return ++MsgEiOpt;

	}

	public static synchronized int getNewMsgOadrPoll() {

		return ++MsgOadrPoll;

	}

//	public static synchronized int getNewActualFlexibility() {
//
//		return ++ActualFlexibility;
//
//	}

	public static synchronized boolean existsActualFlexibility() {

		return (ActualFlexibility == Counter_Singleton_Exists);

	}
	
	public static synchronized void setActualFlexibility() {

		ActualFlexibility = Counter_Singleton_Exists;

	}
	
//	public static synchronized int getNewFlexibilityOffered() {
//
//		return ++FlexibilityOffered;
//
//	}
	
	public static synchronized boolean existsFlexibilityOffered() {

		return (FlexibilityOffered == Counter_Singleton_Exists);

	}
	
	public static synchronized void setFlexibilityOffered() {

		FlexibilityOffered = Counter_Singleton_Exists;

	}
	
//	public static synchronized int getNewBidResult() {
//
//		return ++BidResult;
//
//	}

}
