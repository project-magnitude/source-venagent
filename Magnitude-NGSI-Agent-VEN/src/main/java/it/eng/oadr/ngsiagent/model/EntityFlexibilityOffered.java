package it.eng.oadr.ngsiagent.model;

/**
 * Entity Model
 */
public class EntityFlexibilityOffered extends EntityNGSI {

	private AttributeOadrPayload resourceId;
	private AttributeOadrPayload dateStart;
	private AttributeOadrPayload duration;
	private AttributeOadrPayload flexibilityPrice;
	private AttributeOadrPayload flexibilityOffered;
	private AttributeOadrPayload flexibilityAccepted;
	private AttributeOadrPayload referenceForecast;

	public EntityFlexibilityOffered() {
		super();
	}

	public EntityFlexibilityOffered(String id, String type) {
		super(id, type);
	}

	public EntityFlexibilityOffered(String id, String type, AttributeOadrPayload resourceId,
			AttributeOadrPayload dateStart, AttributeOadrPayload duration, AttributeOadrPayload flexibilityPrice,
			AttributeOadrPayload flexibilityOffered, AttributeOadrPayload flexibilityAccepted,
			AttributeOadrPayload referenceForecast) {
		super(id, type);
		this.resourceId = resourceId;
		this.dateStart = dateStart;
		this.duration = duration;
		this.flexibilityPrice = flexibilityPrice;
		this.flexibilityOffered = flexibilityOffered;
		this.flexibilityAccepted = flexibilityAccepted;
		this.referenceForecast = referenceForecast;
	}

	public AttributeOadrPayload getResourceId() {
		return resourceId;
	}

	public void setResourceId(AttributeOadrPayload resourceId) {
		this.resourceId = resourceId;
	}

	public AttributeOadrPayload getDateStart() {
		return dateStart;
	}

	public void setDateStart(AttributeOadrPayload dateStart) {
		this.dateStart = dateStart;
	}

	public AttributeOadrPayload getDuration() {
		return duration;
	}

	public void setDuration(AttributeOadrPayload duration) {
		this.duration = duration;
	}

	public AttributeOadrPayload getFlexibilityPrice() {
		return flexibilityPrice;
	}

	public void setFlexibilityPrice(AttributeOadrPayload flexibilityPrice) {
		this.flexibilityPrice = flexibilityPrice;
	}

	public AttributeOadrPayload getFlexibilityOffered() {
		return flexibilityOffered;
	}

	public void setFlexibilityOffered(AttributeOadrPayload flexibilityOffered) {
		this.flexibilityOffered = flexibilityOffered;
	}

	public AttributeOadrPayload getFlexibilityAccepted() {
		return flexibilityAccepted;
	}

	public void setFlexibilityAccepted(AttributeOadrPayload flexibilityAccepted) {
		this.flexibilityAccepted = flexibilityAccepted;
	}

	public AttributeOadrPayload getReferenceForecast() {
		return referenceForecast;
	}

	public void setReferenceForecast(AttributeOadrPayload referenceForecast) {
		this.referenceForecast = referenceForecast;
	}

}
