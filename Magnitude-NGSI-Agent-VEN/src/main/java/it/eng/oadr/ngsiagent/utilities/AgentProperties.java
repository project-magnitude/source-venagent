package it.eng.oadr.ngsiagent.utilities;

import java.io.IOException;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.eng.oadr.openadrclient.Oadr20bClient;

public class AgentProperties {

	private static final Logger logger = LoggerFactory.getLogger(AgentProperties.class);
	private static Properties prop = null;

	public static synchronized String getProperty(String key) throws Exception {

		if (prop == null) {
			prop = new Properties();
			try {
				prop.load(AgentProperties.class.getClassLoader().getResourceAsStream("agentVEN.properties"));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		String prefix = "MAGNITUDE_ENV";
		String envKey = prefix + key;
		String value = System.getenv(envKey);
		logger.debug("System.getenv(" + envKey + "): " + value);
		if (value == null) {
			value = prop.getProperty(key);
			logger.debug("prop.getProperty: " + value);
		}

		return value;
	}

}
