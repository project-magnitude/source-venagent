package it.eng.oadr.ngsiagent.listener;

import java.io.IOException;
import java.util.Properties;
import java.util.TimeZone;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import it.eng.oadr.ngsiagent.utilities.Counters;
import it.eng.oadr.ngsiagent.utilities.TokenPEP;

public class QuartzSchedulerListener implements ServletContextListener {

	private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(QuartzSchedulerListener.class);

	public void contextDestroyed(ServletContextEvent arg0) {
		//
	}

	public void contextInitialized(ServletContextEvent arg0) {

		TimeZone utcTimeZone = TimeZone.getTimeZone("UTC");
		TimeZone.setDefault(utcTimeZone);

		// execute Login operation to retrieve Token for next Rest Calls
		executeLogin();

		Properties prop = new Properties();
		try {
			prop.load(this.getClass().getClassLoader().getResourceAsStream("agentVEN.properties"));
		} catch (IOException e) {
			e.printStackTrace();
		}

		if (TokenPEP.isUseTokenPEP()) {
			TokenPEP.generateNewTokenPEP();
		}

		Counters.syncAllCounters();

	}

	private void executeLogin() {
		log.debug("-> executeLogin");

		log.debug("<- executeLogin");
	}

}
