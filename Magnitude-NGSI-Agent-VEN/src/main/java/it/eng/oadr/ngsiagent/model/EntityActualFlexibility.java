package it.eng.oadr.ngsiagent.model;

/**
 * Entity Model
 */
public class EntityActualFlexibility extends EntityNGSI {

	private AttributeOadrPayload resourceId;
	private AttributeOadrPayload dateStart;
	private AttributeOadrPayload duration;
	private AttributeOadrPayload flexibilityProvider;
	private AttributeOadrPayload serviceType;
	private AttributeOadrPayload availablePower;
	private AttributeOadrPayload availableEnergy;
	private AttributeOadrPayload rampRate;
	private AttributeOadrPayload rampDuration;
	private AttributeOadrPayload startTime;
	private AttributeOadrPayload endTime;

	public EntityActualFlexibility() {
		super();
	}

	public EntityActualFlexibility(String id, String type) {
		super(id, type);
	}

	public EntityActualFlexibility(String id, String type, AttributeOadrPayload resourceId,
			AttributeOadrPayload dateStart, AttributeOadrPayload duration, AttributeOadrPayload flexibilityProvider,
			AttributeOadrPayload serviceType, AttributeOadrPayload availablePower, AttributeOadrPayload availableEnergy,
			AttributeOadrPayload rampRate, AttributeOadrPayload rampDuration, AttributeOadrPayload startTime,
			AttributeOadrPayload endTime) {
		super(id, type);
		this.resourceId = resourceId;
		this.dateStart = dateStart;
		this.duration = duration;
		this.flexibilityProvider = flexibilityProvider;
		this.serviceType = serviceType;
		this.availablePower = availablePower;
		this.availableEnergy = availableEnergy;
		this.rampRate = rampRate;
		this.rampDuration = rampDuration;
		this.startTime = startTime;
		this.endTime = endTime;
	}

	public AttributeOadrPayload getResourceId() {
		return resourceId;
	}

	public void setResourceId(AttributeOadrPayload resourceId) {
		this.resourceId = resourceId;
	}

	public AttributeOadrPayload getDateStart() {
		return dateStart;
	}

	public void setDateStart(AttributeOadrPayload dateStart) {
		this.dateStart = dateStart;
	}

	public AttributeOadrPayload getDuration() {
		return duration;
	}

	public void setDuration(AttributeOadrPayload duration) {
		this.duration = duration;
	}

	public AttributeOadrPayload getFlexibilityProvider() {
		return flexibilityProvider;
	}

	public void setFlexibilityProvider(AttributeOadrPayload flexibilityProvider) {
		this.flexibilityProvider = flexibilityProvider;
	}

	public AttributeOadrPayload getServiceType() {
		return serviceType;
	}

	public void setServiceType(AttributeOadrPayload serviceType) {
		this.serviceType = serviceType;
	}

	public AttributeOadrPayload getAvailablePower() {
		return availablePower;
	}

	public void setAvailablePower(AttributeOadrPayload availablePower) {
		this.availablePower = availablePower;
	}

	public AttributeOadrPayload getAvailableEnergy() {
		return availableEnergy;
	}

	public void setAvailableEnergy(AttributeOadrPayload availableEnergy) {
		this.availableEnergy = availableEnergy;
	}

	public AttributeOadrPayload getRampRate() {
		return rampRate;
	}

	public void setRampRate(AttributeOadrPayload rampRate) {
		this.rampRate = rampRate;
	}

	public AttributeOadrPayload getRampDuration() {
		return rampDuration;
	}

	public void setRampDuration(AttributeOadrPayload rampDuration) {
		this.rampDuration = rampDuration;
	}

	public AttributeOadrPayload getStartTime() {
		return startTime;
	}

	public void setStartTime(AttributeOadrPayload startTime) {
		this.startTime = startTime;
	}

	public AttributeOadrPayload getEndTime() {
		return endTime;
	}

	public void setEndTime(AttributeOadrPayload endTime) {
		this.endTime = endTime;
	}

}
