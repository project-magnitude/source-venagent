package it.eng.oadr.ngsiagent.model;

/**
 * Entity Model
 */
public class EntityOadrPayload extends EntityNGSI {

	private AttributeOadrPayload oadrPayload;

	public EntityOadrPayload() {
		super();
	}

	public EntityOadrPayload(String id, String type) {
		super(id, type);
	}

	public EntityOadrPayload(String id, String type, AttributeOadrPayload oadrPayload) {
		this(id, type);
		this.oadrPayload = oadrPayload;
	}

	public AttributeOadrPayload getOadrPayload() {
		return oadrPayload;
	}

	public void setOadrPayload(AttributeOadrPayload oadrPayload) {
		this.oadrPayload = oadrPayload;
	}

}
